da_fill_timestamp 8.x-1.x
---------------

### About this Module

The da_fill_timestamp module updates data on node save for certain content types.

The primary use case for this module is to:

- **Populate timestamp field** with data extracted from node publishing date or from another date field.
- **Be used in bulk operations**. In a site with many content in it, it is not possible to save each node at a time. The core bulk *node save action* available in /admin/content lets save multiple nodes at once. It is possible to create a custom view to save more nodes at a time.

### Goals

- Populate secondary timestamp field to be used as contextual range filter using Drupal 8 [Views Contextual Range Filter](https://www.drupal.org/project/contextual_range_filter).
- This secondary field is needed because the Views Contextual Range Filter 8.x-1.0-rc1 doesn't work properly with date_fields. See [issue #2944490 ](https://www.drupal.org/project/contextual_range_filter/issues/2944490) for more information.
